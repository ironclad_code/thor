﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)_!!!*Q(C=\&gt;7^=2J"'-&lt;RRRY(4OH!1QNP#S1KY!);?&amp;MA*347C/!JQ!ER'&lt;'SNQ53&amp;5!,[,_L(91^9Z(9(IV'N_T&gt;]NR_`(2XH+3_@:?_[8RLW\_Z=&lt;Z^7KMVWP\F?'G0=X]K@@$Z&gt;&gt;TP\8\=H[^WFT\^]\L!6@:L_WX`D5W@W_@WM&lt;@VT2`"0`]&gt;@&amp;&amp;\%6',;GJ31WX:=Z%8?:%8?:%8O=F.&lt;H+4G^TE3:\E3:\E3:\E12\E12\E12\EP:#,8/1CBV1M8CR54&amp;J-5(3'IG*8?!J0Y3E]@&amp;8B+4S&amp;J`!5(LKI]"3?QF.Y#A`$6(A+4_%J0)7(K&lt;KE?C((5XC98M:D0-:D0-&lt;$ED)?!T#,G9H.*$"E4JK']2C0]&gt;#5]2C0]2C0]8";RG-]RG-]RM/1@F8=.;W1YW%;*:\%EXA34_*B;C7?R*.Y%E`C94EFHM34)*)&amp;E]EB+"G5&gt;%C_**\%QU'**`%EHM34?$D6\V$W+^-UL:$D#4S"*`!%HM$$&amp;!I]A3@Q"*\!Q\1+0)%H]!3?Q-.3#DS"*`!%E'"2FF=Q74!Q["1%A9&gt;0@VKCXS6X3@1CD9&gt;8Y[(5?.AU(C+.BU0DJGP=4)W&lt;J((R.3[KRM83O!A;@ZQ'7A/DM9D'Y.:2*`:([I'[IW[I+_J%86$HV&amp;E&lt;_J=\HEYH(9^((1Y(\89\&lt;49&lt;L69L4&gt;/ER7+B_8SOW7RW?1X]I&amp;R?##`PJ48N`8+Z@74-T_7UP8`9XDU^_+\F`_(`]TNY._KLLN@A'DU$B7?UZ!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="TypeDefs" Type="Folder">
		<Item Name="Cluster_CMD_ACT_Data.ctl" Type="VI" URL="../Cluster_CMD_ACT_Data.ctl"/>
		<Item Name="ENUM_ACT_Action.ctl" Type="VI" URL="../ENUM_ACT_Action.ctl"/>
		<Item Name="ENUM_ACT_State.ctl" Type="VI" URL="../ENUM_ACT_State.ctl"/>
	</Item>
	<Item Name="Globals" Type="Folder">
		<Item Name="Global_CMD_ACT_Data In.vi" Type="VI" URL="../Global_CMD_ACT_Data In.vi"/>
		<Item Name="Global_ACT_Data Out.vi" Type="VI" URL="../Global_ACT_Data Out.vi"/>
	</Item>
	<Item Name="Set Drives" Type="Folder">
		<Item Name="NEW Set Drive Motors.vi" Type="VI" URL="../NEW Set Drive Motors.vi"/>
		<Item Name="Set Drive Motors.vi" Type="VI" URL="../Set Drive Motors.vi"/>
		<Item Name="Set Steer Motors.vi" Type="VI" URL="../Set Steer Motors.vi"/>
		<Item Name="NEW_Set Steer Motors.vi" Type="VI" URL="../NEW_Set Steer Motors.vi"/>
		<Item Name="Set Motor Speed.vi" Type="VI" URL="../Set Motor Speed.vi"/>
		<Item Name="Set Encoder Counts.vi" Type="VI" URL="../Set Encoder Counts.vi"/>
	</Item>
	<Item Name="Stream Data" Type="Folder">
		<Item Name="Stream ALL Velocity Values.vi" Type="VI" URL="../Stream ALL Velocity Values.vi"/>
		<Item Name="Stream Output Values.vi" Type="VI" URL="../Stream Output Values.vi"/>
		<Item Name="Stream Target Velocity Values.vi" Type="VI" URL="../Stream Target Velocity Values.vi"/>
		<Item Name="Stream Velocity Values.vi" Type="VI" URL="../Stream Velocity Values.vi"/>
	</Item>
	<Item Name="Utility" Type="Folder">
		<Item Name="Force circular In Range (DBL).vi" Type="VI" URL="../Force circular In Range (DBL).vi"/>
		<Item Name="Find Opposite Point.vi" Type="VI" URL="../Find Opposite Point.vi"/>
		<Item Name="Module Invert Settings.vi" Type="VI" URL="../Module Invert Settings.vi"/>
	</Item>
	<Item Name="Convert" Type="Folder">
		<Item Name="Convert Position Real to Target.vi" Type="VI" URL="../Convert Position Real to Target.vi"/>
		<Item Name="Convert FPS to Counts per 100ms.vi" Type="VI" URL="../Convert FPS to Counts per 100ms.vi"/>
		<Item Name="Convert RPM to FPS.vi" Type="VI" URL="../Convert RPM to FPS.vi"/>
		<Item Name="Convert XY To Polar Velocities.vi" Type="VI" URL="../Convert XY To Polar Velocities.vi"/>
		<Item Name="Convert Polar To  to XY Velocties.vi" Type="VI" URL="../Convert Polar To  to XY Velocties.vi"/>
		<Item Name="Convert Radians to Unit Circle.vi" Type="VI" URL="../Convert Radians to Unit Circle.vi"/>
		<Item Name="Convert Unit Circle to Radians.vi" Type="VI" URL="../Convert Unit Circle to Radians.vi"/>
		<Item Name="Convert to Field  Orientation.vi" Type="VI" URL="../Convert to Field  Orientation.vi"/>
		<Item Name="Convert FPS to CP100ms.vi" Type="VI" URL="../Convert FPS to CP100ms.vi"/>
	</Item>
	<Item Name="Caclulate" Type="Folder">
		<Item Name="Calculate Optimum Steer.vi" Type="VI" URL="../Calculate Optimum Steer.vi"/>
		<Item Name="Calc Shortest Difference.vi" Type="VI" URL="../Calc Shortest Difference.vi"/>
		<Item Name="Invert if in Avoid Zone.vi" Type="VI" URL="../Invert if in Avoid Zone.vi"/>
		<Item Name="Optomize Settings.vi" Type="VI" URL="../Optomize Settings.vi"/>
		<Item Name="Merge Data Sets.vi" Type="VI" URL="../Merge Data Sets.vi"/>
		<Item Name="Calc Frame Data .vi" Type="VI" URL="../Calc Frame Data .vi"/>
		<Item Name="Combine Vectors.vi" Type="VI" URL="../Combine Vectors.vi"/>
		<Item Name="Swerve Calculate Angle and Velocity.vi" Type="VI" URL="../Swerve Calculate Angle and Velocity.vi"/>
		<Item Name="Swerve Data Normalize Velocity.vi" Type="VI" URL="../Swerve Data Normalize Velocity.vi"/>
	</Item>
	<Item Name="Reference" Type="Folder">
		<Item Name="FieldOrientedTransform.vi" Type="VI" URL="../FieldOrientedTransform.vi"/>
		<Item Name="Calc Traverse Components .vi" Type="VI" URL="../Calc Traverse Components .vi"/>
	</Item>
	<Item Name="ACT_Loop.vi" Type="VI" URL="../ACT_Loop.vi"/>
	<Item Name="Swerve Balance To Level PID.vi" Type="VI" URL="../Swerve Balance To Level PID.vi"/>
	<Item Name="Build Rotation Set.vi" Type="VI" URL="../Build Rotation Set.vi"/>
	<Item Name="Build Traverse Set.vi" Type="VI" URL="../Build Traverse Set.vi"/>
	<Item Name="Build Actual View.vi" Type="VI" URL="../Build Actual View.vi"/>
	<Item Name="Seperate at Decimal Point.vi" Type="VI" URL="../Seperate at Decimal Point.vi"/>
	<Item Name="Swerve_Simple Config.vi" Type="VI" URL="../Swerve config/Swerve_Simple Config.vi"/>
	<Item Name="Config File Name.vi" Type="VI" URL="../../../../Shared Code/Config/Config File Name.vi"/>
	<Item Name="Swerve Rotate To Angle PID.vi" Type="VI" URL="../Swerve Rotate To Angle PID.vi"/>
	<Item Name="Auto Angle.vi" Type="VI" URL="../Auto Angle.vi"/>
	<Item Name="Convert Counts to Feet.vi" Type="VI" URL="../Convert Counts to Feet.vi"/>
	<Item Name="Conver Encoder distance to ft.vi" Type="VI" URL="../Conver Encoder distance to ft.vi"/>
	<Item Name="Untitled 2.vi" Type="VI" URL="../Untitled 2.vi"/>
	<Item Name="Auto PID Drive.vi" Type="VI" URL="../Auto PID Drive.vi"/>
	<Item Name="Detect CMD Change.vi" Type="VI" URL="../Detect CMD Change.vi"/>
</Library>
