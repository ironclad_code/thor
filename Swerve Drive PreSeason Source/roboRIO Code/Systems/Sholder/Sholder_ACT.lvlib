﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)_!!!*Q(C=\&gt;5R4A*"&amp;-&lt;R4W."SQU-6XB8I,1SY1LP#L37B.A:CW&gt;J37H,&amp;6ZF9G("$1R8Q0]-$U+-A5;.-=Y[S_YX/T-`:J&gt;6[G5A87BTKAS/FN:/&lt;@M[/1DX&lt;&gt;PWX;\P[W3T[\HZW,\P@^DVM`[\)8&lt;T(ES]04TO0V(U8`\,XSYX*X]%X`Y\/&amp;.\%6'4'F3HGNKUGS20]C20]C20]C!0]C!0]C!0=C&gt;X=C&gt;X=C&gt;X=C-X=C-X=C-X]L[2CVTE)I?54*Z-F!S;$*"=$%8*,P%EHM34?$B6YEE]C3@R*"YO5?**0)EH]31?OCHR**\%EXA3$U.V3@;.(%`C98A&amp;HM!4?!*0Y'&amp;+":Y!%%Q7$"Q-!E."9X!1?!*0Y/&amp;1A3@Q"*\!%XBI6O!*0)%H]!1?OP26C;ZJ'TE?BJ(D=4S/R`%Y(I;7YX%]DM@R/"[GE_.R0!\#G&gt;!:()+=4MY&amp;TIHD=4R]S0%Y(M@D?"Q04@U/?6_:JGE&lt;/2\$9XA-D_%R0!QBQW.Y$)`B-4Q-+].D?!S0Y4%]4#8$9XA-DQ%R*G6['9-:(9W,D-$Q].?@&amp;ONX+&lt;L%_C&lt;6Q[N[+&amp;50G_IB5DU=KJOOOJGKG[2;@.7CKB:,N1CK,[&gt;#KT#K363&gt;WY6;MV^2F^1&amp;&gt;5;&gt;5C@5-86%(&lt;;O8XTB?LX7;L83=LH59L(1&lt;$&lt;4&gt;$L6:$,2?$T7;$43=$D=PQ9OW@9PB/V\[9&lt;DB[P&lt;V]@L_@X&gt;]`RVTO@4V&gt;N,SX`A``-P?$@K8)&gt;TM%&lt;PT_658A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="ACT_Loop.vi" Type="VI" URL="../ACT_Loop.vi"/>
	<Item Name="Outputs HW Read.vi" Type="VI" URL="../Outputs HW Read.vi"/>
	<Item Name="Global_CMD_ACT_Data In.vi" Type="VI" URL="../Global_CMD_ACT_Data In.vi"/>
	<Item Name="Global_ACT_Data Out.vi" Type="VI" URL="../Global_ACT_Data Out.vi"/>
	<Item Name="Cluster_CMD_ACT_Data.ctl" Type="VI" URL="../Cluster_CMD_ACT_Data.ctl"/>
	<Item Name="ENUM_ACT_Action.ctl" Type="VI" URL="../ENUM_ACT_Action.ctl"/>
	<Item Name="ENUM_ACT_State.ctl" Type="VI" URL="../ENUM_ACT_State.ctl"/>
	<Item Name="Outputs FX Read.vi" Type="VI" URL="../Outputs FX Read.vi"/>
	<Item Name="Outputs SW Set.vi" Type="VI" URL="../Outputs SW Set.vi"/>
	<Item Name="Config CanCoder Set.vi" Type="VI" URL="../Config CanCoder Set.vi"/>
	<Item Name="Config FX Extract.vi" Type="VI" URL="../Config FX Extract.vi"/>
	<Item Name="Config HW Extract.vi" Type="VI" URL="../Config HW Extract.vi"/>
	<Item Name="Config HW Set.vi" Type="VI" URL="../Config HW Set.vi"/>
	<Item Name="Config FX Set.vi" Type="VI" URL="../Config FX Set.vi"/>
	<Item Name="Manual Override.vi" Type="VI" URL="../Manual Override.vi"/>
</Library>
