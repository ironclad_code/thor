﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)&lt;!!!*Q(C=\&gt;4"&lt;=*!%)8BFSC(8#H!5E1,LQ6;I)6JA7O/N$!NU!)N4!OU1!P/\W6!53+&amp;3S*&amp;5&gt;::!]_\MV`7"GGU:_F*]\UW@&gt;EO!_&lt;R/HK@JX';,B5O_83.L]&amp;]P4R^OH[&gt;-N`K@\D_LPZN`9Y`L(`8`X84@`NP@\O^XPU3`0DXY%(,$R'^[%E0OL5M/R&gt;ZE2&gt;ZE2&gt;ZE3&gt;ZEC&gt;ZEC&gt;ZEA&gt;ZE!&gt;ZE!&gt;ZE*P=Z#9XO=F.0AZSE9N=Z*#+R9O&amp;CK*&amp;A7)Q&amp;"7HQF.Y#E`BY;-+4_%J0)7H]$"%B;@Q&amp;*\#5XC9JM*4?!J0Y3E]F"K3'A=ZHM*$?37?R*.Y%E`C95EFHA31,*954IL!5()R?:.Y%E`CY;U34_**0)EH]8":C3@R**\%EXC9-H9FBW9ZS0&amp;12I%H]!3?Q".Y++X!%XA#4_!*0#SHQ".Y!E3Q9&amp;!=AI**Q9$A1_!*0,QI]!3?Q".Y!A_8RBW+M4/,:DH)]2C0]2C0]2A0*71]RG-]RG-]F*8R')`R')`RM*3-RXC-RU$-ICQP5]R-.).-9$T]D;@&amp;YS\FE(A=5D_][I&gt;3`&lt;#J(S,VQ['_[?K&lt;K&lt;Z*[MV8&lt;[J[M^3&lt;I0\HV'AV2LW)?P)S5'@/*`K2@K$P[4P[FL[BL_GL:?IX$TS@TTK&gt;4DI?DTI=$NLP^^LN&gt;NJON^JM.FKPVVKN6L?@A2?/WQ`#&lt;`B&gt;_B7'2\V@ATV[!RM`?#Y!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="CMD_Loop.vi" Type="VI" URL="../CMD_Loop.vi"/>
	<Item Name="Global_CMD_CMD_Data In.vi" Type="VI" URL="../Global_CMD_CMD_Data In.vi"/>
	<Item Name="Global_CMD_Data Out.vi" Type="VI" URL="../Global_CMD_Data Out.vi"/>
	<Item Name="Cluster_CMD_CMD_Data.ctl" Type="VI" URL="../Cluster_CMD_CMD_Data.ctl"/>
	<Item Name="ENUM_CMD_Action.ctl" Type="VI" URL="../ENUM_CMD_Action.ctl"/>
	<Item Name="ENUM_CMD_State.ctl" Type="VI" URL="../ENUM_CMD_State.ctl"/>
	<Item Name="ENUM_CMD_Mode.ctl" Type="VI" URL="../ENUM_CMD_Mode.ctl"/>
	<Item Name="ENUM_Pose List.ctl" Type="VI" URL="../ENUM_Pose List.ctl"/>
	<Item Name="Build Pose Target Array.vi" Type="VI" URL="../Build Pose Target Array.vi"/>
	<Item Name="Determine Current Pose.vi" Type="VI" URL="../Determine Current Pose.vi"/>
</Library>
