﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*%!!!*Q(C=\&gt;5^&lt;BJ"'-&lt;RRV%+N^QAYALP&amp;3B&gt;2;*._6["VC6&amp;KEAOXC*&amp;CD1LZ1#)+TRN#B&gt;=A3NM`D--S)IMUTB2&amp;(H'!_ST]`&amp;D&gt;FF,P&gt;R+\T6@+\M83TP@/P6_JY.,/\`V@LNT@KGHN`.,0^\.TY]@1_:Z@G&lt;]EWF`'T]/8`:@+8IL&lt;_8`,P&gt;8@Q2``(&gt;QI`9AIJF7N+3&amp;WL+TS5VO=J/&lt;X/2&amp;8O2&amp;8O2&amp;8O2*HO2*HO2*HO2"(O2"(O2"(O3^EIN=Z#+(:"9X#ZF*T13G-R3:&amp;_-R(O-R(AZF0-:D0-:D0(32]2C0]2C0]4"-RG-]RG-]RM.58?*?S@%9$^/L]"3?QF.Y#A^,KP!5A'+R9O*C%BAK4B9@#E`B+4R]6/%J0)7H]"1?4KPQ&amp;*\#5XA+$U0[LF48N%K/BWG5?"*0YEE]C9?JF8A34_**0)G(Z:2Y%E_#3":-*I?A:&amp;$3)4F)0)G(.S7?R*.Y%E`CY63`1NFXJGF;*==4?!*0Y!E]A9=J&amp;(A#4_!*0)'(;26Y!E`A#4S"B[55?!*0Y!EAQ;)MLW#S9'$1+1A#$X`^&lt;IF_F;J,IF&gt;JX,Q;.[8'T;:R%WH=("I88?.C;FQED=X8W&amp;3.T&gt;,9")UPJY(7Q'AMID'Y&gt;&gt;32VQ.N4ZNI7^K'NK;N;%P;IAV^Z9\(YV'(QU(\`6\4.'G\X7KTW7C^8GOV7GGZ8'KR7&amp;Q?!R_IFQ@#[&lt;FUT_?PH`XY\7Z[_0(&amp;D^]`41`VU4^&lt;`B@_0`]$TU;^U^-VW+.@W33]"A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="ACT_Loop.vi" Type="VI" URL="../ACT_Loop.vi"/>
	<Item Name="Global_CMD_ACT_Data In.vi" Type="VI" URL="../Global_CMD_ACT_Data In.vi"/>
	<Item Name="Global_ACT_Data Out.vi" Type="VI" URL="../Global_ACT_Data Out.vi"/>
	<Item Name="Cluster_CMD_ACT_Data.ctl" Type="VI" URL="../Cluster_CMD_ACT_Data.ctl"/>
	<Item Name="ENUM_ACT_Action.ctl" Type="VI" URL="../ENUM_ACT_Action.ctl"/>
	<Item Name="ENUM_ACT_State.ctl" Type="VI" URL="../ENUM_ACT_State.ctl"/>
	<Item Name="Outputs FX Read.vi" Type="VI" URL="../Outputs FX Read.vi"/>
	<Item Name="Outputs SW Set.vi" Type="VI" URL="../Outputs SW Set.vi"/>
	<Item Name="Manual Override.vi" Type="VI" URL="../Manual Override.vi"/>
	<Item Name="Config FX Extract.vi" Type="VI" URL="../Config FX Extract.vi"/>
	<Item Name="Config FX Set.vi" Type="VI" URL="../Config FX Set.vi"/>
</Library>
