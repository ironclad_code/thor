﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*&amp;!!!*Q(C=\&gt;5^4M.!%)&lt;B$U2"GROAC"J&amp;GCPY#LH#&amp;,Z!7AK+)('"+3H3J++&amp;X!$.&amp;&gt;*4Z1LQ?DW#)#43!%+).2PM&lt;`]?VI[27DG64P2]K-Q_,&lt;4X@4`L/?X(MX&gt;8L8UW8L&lt;0]6&gt;@14_/(TN5?\8NN8]=X\K^D&gt;^P`,D_!@_"IP`S8`ZWO4TY*@DW\]'2BB=2.;F"&gt;;JJ709ZS:-]S:-]S:-]S)-]S)-]S)0=S:X=S:X=S:X=S)X=S)X=S)W](?1C&amp;\H))37,*QMFES94**WB+0F)0)EH]31?,J6Y%E`C34S*BSZ+0)EH]33?R--Q*:\%EXA34_*BKC&lt;*&gt;J$D34R-L]!4?!*0Y!E],+H!%Q##R9+*AUFA+'A-4A*0Y!E]H#LQ"*\!%XA#$]U+0)%H]!3?Q-/1NCP2.-."DI&gt;JZ(A=D_.R0)[(K?6Y()`D=4S/B_8E?"S0AX!7&gt;#;()'?1U]'Z=$S/BV^S0)\(]4A?RU.4OU0?&gt;G&lt;1$!=Z(M.D?!S0Y4%]4#($9XA-D_%R0%QLQW.Y$)`B-4QM*=.D?!S0!4%7:8E:ERE$D5Z'9(DY;5_,N&lt;M547,NE/LB64W5KI&gt;.^2#J(A\646@&gt;4.6.5GW_;F.6G[8;".5@JU+L-+J&amp;6)/(DNLRO;6OK'PKELKATKE&gt;&gt;5K&gt;$%/`O/.ON^.WO^6GM^&amp;[P&gt;:SO&gt;2CM&gt;"]0F@8&gt;:J/JZJ-*K_PA4//VR@#_&amp;[[Z0TBYOH_NLN:X6U^LG[\[^8$_&gt;0^E0`!`_&gt;@]'\5M@&lt;89)^?!%.-241!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="ACT_Loop.vi" Type="VI" URL="../ACT_Loop.vi"/>
	<Item Name="Global_CMD_ACT_Data In.vi" Type="VI" URL="../Global_CMD_ACT_Data In.vi"/>
	<Item Name="Global_ACT_Data Out.vi" Type="VI" URL="../Global_ACT_Data Out.vi"/>
	<Item Name="Cluster_CMD_ACT_Data.ctl" Type="VI" URL="../Cluster_CMD_ACT_Data.ctl"/>
	<Item Name="ENUM_ACT_Action.ctl" Type="VI" URL="../ENUM_ACT_Action.ctl"/>
	<Item Name="ENUM_ACT_State.ctl" Type="VI" URL="../ENUM_ACT_State.ctl"/>
	<Item Name="Outputs HW Read.vi" Type="VI" URL="../Outputs HW Read.vi"/>
	<Item Name="Outputs SW Set.vi" Type="VI" URL="../Outputs SW Set.vi"/>
	<Item Name="Config FX Extract.vi" Type="VI" URL="../Config FX Extract.vi"/>
	<Item Name="Config FX Set.vi" Type="VI" URL="../Config FX Set.vi"/>
	<Item Name="Config HW Set.vi" Type="VI" URL="../Config HW Set.vi"/>
	<Item Name="Config CanCoder Set.vi" Type="VI" URL="../Config CanCoder Set.vi"/>
	<Item Name="Config HW Extract.vi" Type="VI" URL="../Config HW Extract.vi"/>
	<Item Name="Outputs FX Read.vi" Type="VI" URL="../Outputs FX Read.vi"/>
	<Item Name="Manual Override.vi" Type="VI" URL="../Manual Override.vi"/>
</Library>
